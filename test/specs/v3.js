const BASE_URL = "https://sit.veservecompany.com";
const GLOBAL_TIMEOUT = 15000;

describe("[T01] V3 Basic", () => {
    it("Can login with user Joeson.", () => {
        browser.url(`${BASE_URL}/chatbot-cms/login`);
        $("input#username").setValue("joeson");
        $("input#password").setValue("123456");
        $("button[type='submit']").click();
        browser.waitUntil(() => {
            let url = browser.getUrl();
            return url === `${BASE_URL}/chatbot-cms/`;
        }, { timeout: GLOBAL_TIMEOUT, timeoutMsg: `Expected to be logged in within ${GLOBAL_TIMEOUT}ms.` });
    });

    it("Navigate to KB Tree", () => {
        $("a[href='/chatbot-cms/kb/advanced-tree']").waitForExist({ timeout: GLOBAL_TIMEOUT });
        $("a[href='/chatbot-cms/kb/advanced-tree']").click();
        $(".block-body").waitForExist({ timeout: GLOBAL_TIMEOUT });
    });
});